open Mirage

let main =
  foreign
    ~packages:[package "duration"; package "lwt_react"]
    "Unikernel.Hello" (time @-> job)

let () =
  register "hello" [main $ default_time]
