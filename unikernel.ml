open Lwt.Infix

module Hello (Time : Mirage_time_lwt.S) = struct

  let start _time =
    let hello, run =
      let open Lwt_react in
      let e, send = E.create () in
      let run () =
        let rec loop n =
          if n > 0
          then begin
            send "hello";
            Time.sleep_ns (Duration.of_sec 1) >>= fun () ->
            loop (n-1)
          end else Lwt.return_unit in
        loop 4 in
      e, run in
    let printer = Lwt_react.E.map (fun s -> Logs.info (fun f -> f "%s" s)) hello in

    run ()

end
